package cz.codexis.documentpopularityservice;

import cz.codexis.documentpopularityservice.model.VisitedDocument;
import cz.codexis.documentpopularityservice.service.IVisitedDocumentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(ResourceUtils.CLASSPATH_URL_PREFIX + "application-test.properties")
class DocumentPopularityApplicationTests {

  private final UUID docId1 = UUID.randomUUID();
  private final UUID docId2 = UUID.randomUUID();
  private final UUID docId3 = UUID.randomUUID();
  private final UUID docId4 = UUID.randomUUID();
  private final UUID docId5 = UUID.randomUUID();


  @Autowired
  IVisitedDocumentService visitedDocumentService;

  @BeforeEach
  public void setUp() {
    List<VisitedDocument> visitedDocuments = new ArrayList<>();
    for (int i = 0; i < 20; i++) {
      visitedDocuments.add(new VisitedDocument(UUID.randomUUID(), docId1, UUID.randomUUID(), LocalDateTime.now().minusHours(i)));
    }
    for (int i = 2; i < 12; i++) {
      visitedDocuments.add(new VisitedDocument(UUID.randomUUID(), docId2, UUID.randomUUID(), LocalDateTime.now().minusDays(i)));
    }
    for (int i = 0; i < 30; i++) {
      visitedDocuments.add(new VisitedDocument(UUID.randomUUID(), docId3, UUID.randomUUID(), LocalDateTime.now().minusHours(i)));
    }
    for (int i = 0; i < 8; i++) {
      visitedDocuments.add(new VisitedDocument(UUID.randomUUID(), docId4, UUID.randomUUID(), LocalDateTime.now().minusDays(i)));
    }
    for (int i = 0; i < 50; i++) {
      visitedDocuments.add(new VisitedDocument(UUID.randomUUID(), docId5, UUID.randomUUID(), LocalDateTime.now().minusMinutes(i)));
    }

    visitedDocumentService.createMultiple(visitedDocuments);
  }

  @Test
  public void getPopularDocuments() {
    // time interval is last week
    List<UUID> result = visitedDocumentService.findPopularDocuments(LocalDateTime.now().minusWeeks(1), LocalDateTime.now(), 4);

    Assertions.assertEquals(4, result.size());
    Assertions.assertEquals(docId5, result.get(0));
    Assertions.assertEquals(docId3, result.get(1));
    Assertions.assertEquals(docId1, result.get(2));
    Assertions.assertEquals(docId4, result.get(3));
  }
}
