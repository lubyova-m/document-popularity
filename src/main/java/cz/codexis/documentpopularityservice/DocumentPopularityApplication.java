package cz.codexis.documentpopularityservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentPopularityApplication {

  public static void main(String[] args) {
    SpringApplication.run(DocumentPopularityApplication.class, args);
  }
}
