package cz.codexis.documentpopularityservice.repository;

import cz.codexis.documentpopularityservice.model.VisitedDocument;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface VisitedDocumentRepository extends JpaRepository<VisitedDocument, UUID> {

  @Query("SELECT v.documentId, count(v) as docsCount FROM visited v WHERE v.time BETWEEN :timeFrom AND :timeTo GROUP BY v.documentId ORDER BY docsCount DESC")
  List<Object[]> findMostPopularDocsVisitedInTimeInterval(@Param("timeFrom") LocalDateTime timeFrom, @Param("timeTo") LocalDateTime timeTo, Pageable pageable);
}
