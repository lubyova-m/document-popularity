package cz.codexis.documentpopularityservice.controller;

import cz.codexis.documentpopularityservice.model.Document;

import java.util.List;

import cz.codexis.documentpopularityservice.service.IDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/documents")
public class DocumentController {

  @Autowired
  IDocumentService documentService;

  @PostMapping("/create")
  public Document createDocument(Document document) {
    return documentService.create(document);
  }

  @GetMapping("/list")
  public List<Document> getDocuments() {
    return documentService.findAll();
  }
}
