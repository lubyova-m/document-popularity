package cz.codexis.documentpopularityservice.controller;

import cz.codexis.documentpopularityservice.model.VisitedDocument;
import cz.codexis.documentpopularityservice.service.IVisitedDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/visited")
public class VisitedDocumentController {

  @Autowired
  IVisitedDocumentService visitedDocumentService;

  @PostMapping("/create")
  public VisitedDocument createVisitedDocument(VisitedDocument visitedDocument) {
    return visitedDocumentService.create(visitedDocument);
  }

  @PostMapping("/list")
  public List<VisitedDocument> getVisitedDocuments() {
    return visitedDocumentService.findAll();
  }

  @GetMapping("/listPopularDocuments")
  public List<UUID> getPopularDocuments(LocalDateTime timeFrom, LocalDateTime timeTo, Integer numberOfDocs) {
    return visitedDocumentService.findPopularDocuments(timeFrom, timeTo, numberOfDocs);
  }

}
