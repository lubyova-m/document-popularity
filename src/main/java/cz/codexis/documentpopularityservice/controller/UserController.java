package cz.codexis.documentpopularityservice.controller;

import cz.codexis.documentpopularityservice.model.User;
import cz.codexis.documentpopularityservice.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

  @Autowired
  private IUserService userService;

  @PostMapping("/create")
  public User createUser(User user) {
    return userService.create(user);
  }

  @GetMapping("/list")
  public List<User> getUsers() {
    return userService.findAll();
  }
}
