package cz.codexis.documentpopularityservice.model;

import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "visited")
@Table(name = "visited")
public class VisitedDocument {

  @Id
  private UUID id;

  private UUID documentId;

  private UUID userId;

  private LocalDateTime time;

  public VisitedDocument() {

  }

  public VisitedDocument(UUID id, UUID documentId, UUID userId, LocalDateTime time) {
    this.id = id;
    this.documentId = documentId;
    this.userId = userId;
    this.time = time;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getDocumentId() {
    return documentId;
  }

  public void setDocumentId(UUID documentId) {
    this.documentId = documentId;
  }

  public UUID getUserId() {
    return userId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  public LocalDateTime getTime() {
    return time;
  }

  public void setTime(LocalDateTime time) {
    this.time = time;
  }
}
