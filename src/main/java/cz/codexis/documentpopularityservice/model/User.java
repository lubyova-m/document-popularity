package cz.codexis.documentpopularityservice.model;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "users")
@Table(name = "users")
public class User {

  @Column(nullable = false)
  @Id
  private UUID id;

  private String login;

  public User() {

  }

  public User(UUID id, String login) {
    this.id = id;
    this.login = login;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }
}
