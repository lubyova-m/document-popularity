package cz.codexis.documentpopularityservice.service;

import cz.codexis.documentpopularityservice.model.Document;

import java.util.List;

public interface IDocumentService {
  Document create(Document document);

  List<Document> findAll();
}
