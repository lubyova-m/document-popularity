package cz.codexis.documentpopularityservice.service;

import cz.codexis.documentpopularityservice.model.VisitedDocument;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface IVisitedDocumentService {
  VisitedDocument create(VisitedDocument visitedDocument);

  List<VisitedDocument> createMultiple(List<VisitedDocument> visitedDocuments);

  List<VisitedDocument> findAll();

  List<UUID> findPopularDocuments(LocalDateTime timeFrom, LocalDateTime timeTo, Integer numberOfDocs);
}
