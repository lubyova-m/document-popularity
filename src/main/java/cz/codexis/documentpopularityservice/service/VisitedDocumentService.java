package cz.codexis.documentpopularityservice.service;

import cz.codexis.documentpopularityservice.model.VisitedDocument;
import cz.codexis.documentpopularityservice.repository.VisitedDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class VisitedDocumentService implements IVisitedDocumentService {

  @Autowired
  private VisitedDocumentRepository visitedDocumentRepository;

  @Override
  public VisitedDocument create(VisitedDocument visitedDocument) {
    return visitedDocumentRepository.save(visitedDocument);
  }

  @Override
  public List<VisitedDocument> createMultiple(List<VisitedDocument> visitedDocuments) {
    return visitedDocumentRepository.saveAll(visitedDocuments);
  }

  @Override
  public List<VisitedDocument> findAll() {
    return visitedDocumentRepository.findAll();
  }

  @Override
  public List<UUID> findPopularDocuments(LocalDateTime timeFrom, LocalDateTime timeTo, Integer numberOfDocs) {
    List<Object[]> visitedDocumentsWithCount = visitedDocumentRepository.findMostPopularDocsVisitedInTimeInterval(timeFrom, timeTo, PageRequest.of(0, numberOfDocs));
    List<UUID> documentIds = new ArrayList<>();
    for (Object[] d : visitedDocumentsWithCount) {
      documentIds.add((UUID) d[0]);
    }
    return documentIds;
  }
}
