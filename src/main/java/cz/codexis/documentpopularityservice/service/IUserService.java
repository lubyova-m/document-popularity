package cz.codexis.documentpopularityservice.service;

import cz.codexis.documentpopularityservice.model.User;

import java.util.List;

public interface IUserService {
  User create(User user);

  List<User> findAll();
}
