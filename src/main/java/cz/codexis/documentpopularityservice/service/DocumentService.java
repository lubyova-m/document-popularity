package cz.codexis.documentpopularityservice.service;

import cz.codexis.documentpopularityservice.model.Document;
import cz.codexis.documentpopularityservice.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentService implements IDocumentService {

  @Autowired
  private DocumentRepository documentRepository;

  @Override
  public Document create(Document document) {
    return documentRepository.save(document);
  }

  @Override
  public List<Document> findAll() {
    return documentRepository.findAll();
  }
}
